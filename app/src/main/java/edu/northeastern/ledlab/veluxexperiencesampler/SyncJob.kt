package edu.northeastern.ledlab.veluxexperiencesampler

import android.content.Context
import android.util.Log
import com.firebase.jobdispatcher.*
import java.util.concurrent.TimeUnit

class SyncJob : JobService() {
    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SyncJob::JobService::onStartJob")

        DBUtils.syncData(this)

        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SyncJob::JobService::onStopJob")

        return false
    }

    companion object {
        private const val TAG = "sync_job"

        fun scheduleJob(context: Context) {
            Log.d("KAKA", "SyncJob::CompanionObject::scheduleJob")

            val timeInSecondsStart = TimeUnit.HOURS.toSeconds(3L).toInt()
            val timeInSecondsEnd = timeInSecondsStart + TimeUnit.MINUTES.toSeconds(30L).toInt()

            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))

            dispatcher.cancelAll()

            val myJob = dispatcher.newJobBuilder()
                    .setService(SyncJob::class.java)
                    .setTag(SyncJob.TAG)
                    .setRecurring(true)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSecondsStart, timeInSecondsEnd))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .setConstraints(
                            Constraint.ON_ANY_NETWORK
                    )
                    .build()

            dispatcher.mustSchedule(myJob)
        }
    }
}