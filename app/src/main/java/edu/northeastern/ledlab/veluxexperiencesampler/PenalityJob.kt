package edu.northeastern.ledlab.veluxexperiencesampler

import android.os.Bundle
import android.util.Log
import com.firebase.jobdispatcher.*
import java.util.concurrent.TimeUnit
import kotlin.math.max

class PenalityJob : JobService() {
    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "NotifyJob::JobService::onStartJob")

        val extras = job!!.extras
        val dateInMillis = extras!!.getLong("dateInMillis", System.currentTimeMillis())

        DBUtils.schedulePenalityJob(this, dateInMillis)

        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "PenalityJob::JobService::onStopJob")

        return true
    }

    companion object {
        private const val TAG = "penalty_job"

        fun scheduleJob(dispatcher: FirebaseJobDispatcher, dateInMillis: Long, sleepTime: Int) {
            val timeInMillis = dateInMillis + TimeUnit.MINUTES.toMillis(sleepTime.toLong())
            val timeInSeconds = TimeUnit.MILLISECONDS.toSeconds(max(60000L, timeInMillis - System.currentTimeMillis())).toInt()

            val extras = Bundle()
            extras.putLong("dateInMillis", dateInMillis)

            val myJob = dispatcher.newJobBuilder()
                    .setService(PenalityJob::class.java)
                    .setTag("${PenalityJob.TAG}$timeInMillis")
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .setExtras(extras)
                    .build()

            dispatcher.mustSchedule(myJob)
        }
    }
}