package edu.northeastern.ledlab.veluxexperiencesampler

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.firebase.jobdispatcher.*
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.max


class SamplingNotifyJob : JobService() {
    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SamplingNotifyJob::JobService::onStartJob")

        val extras = job!!.extras

        val mNotificationId = System.currentTimeMillis().toInt()
        val text = extras!!.getString("text", this.getString(R.string.remainder_b))

        val confirmIntent = Intent(this, NotificationReceiver::class.java)
        confirmIntent.action = System.currentTimeMillis().toString()
        confirmIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        confirmIntent.putExtra("NotificationID", mNotificationId)

        val soundURI = Uri.parse("android.resource://edu.northeastern.ledlab.veluxexperiencesampler/" + R.raw.sampling)

        val mBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(this.getString(R.string.app_name))
                .setContentText(text)
                .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .addAction(R.drawable.ic_check_black_24dp, "OK", PendingIntent.getBroadcast(this, 0, confirmIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSound(soundURI)
                .setVibrate(longArrayOf(1000, 1000))
                .setAutoCancel(true)

        val mNotifyMgr = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotifyMgr.notify(mNotificationId, mBuilder.build())

        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SamplingNotifyJob::JobService::onStopJob")

        return true
    }

    companion object {
        private const val TAG = "sampling_notify_job"

        fun scheduleJob(dispatcher: FirebaseJobDispatcher, timeInMillis: Long, text: String) {
            Log.d("KAKA", "SamplingNotifyJob::CompanionObject::scheduleJob")

            val timeInSeconds = TimeUnit.MILLISECONDS.toSeconds(max(60000L, timeInMillis - System.currentTimeMillis())).toInt()

            val extras = Bundle()
            extras.putString("text", text)

            val myJob = dispatcher.newJobBuilder()
                    .setService(SamplingNotifyJob::class.java)
                    .setTag("${SamplingNotifyJob.TAG}$timeInMillis")
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .setExtras(extras)
                    .build()

            dispatcher.mustSchedule(myJob)
        }

        fun scheduleSamplingNotifications(context: Context, date: Date) {
            Log.d("KAKA", "SamplingNotifyJob::CompanionObject::scheduleJobs")

            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))
            val proofTimes = ArrayList<Int>()

            val wakeUpCalendar = Calendar.getInstance()
            wakeUpCalendar.time = date

            wakeUpCalendar.add(Calendar.MINUTE, 30)
            proofTimes.add(wakeUpCalendar.get(Calendar.HOUR_OF_DAY) * 60 + wakeUpCalendar.get(Calendar.MINUTE))
            scheduleJob(dispatcher, wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_b))

            wakeUpCalendar.add(Calendar.MINUTE, 135)
            proofTimes.add(wakeUpCalendar.get(Calendar.HOUR_OF_DAY) * 60 + wakeUpCalendar.get(Calendar.MINUTE))
            scheduleJob(dispatcher, wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_c))

            wakeUpCalendar.add(Calendar.MINUTE, 600)
            proofTimes.add(wakeUpCalendar.get(Calendar.HOUR_OF_DAY) * 60 + wakeUpCalendar.get(Calendar.MINUTE))

            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)

            val firstDateCalendar = Calendar.getInstance()
            firstDateCalendar.time = timeMapObj.samplingStartDate

            if (simpleDateFormat.format(wakeUpCalendar.time) == simpleDateFormat.format(firstDateCalendar.time))
                scheduleJob(dispatcher, wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_d))
            else
                scheduleJob(dispatcher, wakeUpCalendar.timeInMillis, context.getString(R.string.remainder_c))

            val currDate = simpleDateFormat.format(date.time)
            val dateMap = timeMapObj.timeMap[currDate]
            val newDateMap = DayMap(dateMap!!.surveyTimes,
                    dateMap.completedSurveys,
                    dateMap.completedProofs,
                    proofTimes,
                    dateMap.wakeUpTime,
                    dateMap.sleepHours)
            timeMapObj.timeMap[currDate] = newDateMap

            val json = Gson().toJson(timeMapObj)
            val prefEditor = sharedPref!!.edit()
            prefEditor.putString(context.getString(R.string.time_map_key), json)
            prefEditor.apply()
        }
    }
}