package edu.northeastern.ledlab.veluxexperiencesampler

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmResults
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by ledlab on 8/20/17.
 */
object DBUtils {

    private fun updateUI(context: Context, msg: String) {
        val activity = context as Activity
        activity.runOnUiThread({
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            activity.finish()
        })
    }

    private fun getDatabase(): FirebaseFirestore {

        return FirebaseFirestore.getInstance()
    }

    private fun loadMainActivity(activity: Activity, msg: String) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        activity.startActivity(intent)
        updateUI(activity, msg)
    }

    fun registerUser(activity: Activity) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val participantId = sharedPref.getString(activity.getString(R.string.pid), "unknown")

        val db = DBUtils.getDatabase()
        val docRef = db.collection("Users").document(participantId)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                val prefEditor = sharedPref.edit()
                if (document != null && document.exists()) {
                    prefEditor.putBoolean(activity.getString(R.string.preg), true)
                    val burst = (document.data!!["Burst"] as Long).toInt()
                    prefEditor.putInt("Burst", burst)
                    val calibration = document.data!!["Calibration"]
                    if (calibration != null)
                        prefEditor.putInt("MeanSetSize", ((calibration as Map<String, Any>)["MeanSetSize"] as Double).toInt())
                    val totalSurveys = (document.data!!["TotalSurveys"] as Long).toInt()
                    prefEditor.putInt("TotalSurveys", totalSurveys)
                    val totalProofs = (document.data!!["TotalProofs"] as Long).toInt()
                    prefEditor.putInt("TotalProofs", totalProofs)
                    var goals = (document.data!!["Goals"] as Map<String, String>)
                    val burstData = document.data!!["Burst$burst"]
                    if (burstData != null)
                        goals = (document.data!!["Burst$burst"] as Map<String, Any>)["Goals"] as Map<String, String>

                    prefEditor.putBoolean("SalivaSampling", document.data!!["SalivaSampling"] as Boolean)
                    prefEditor.putBoolean("HairSampling", document.data!!["HairSampling"] as Boolean)

                    prefEditor.putString("Goal1", goals["Goal1"])
                    prefEditor.putString("Goal2", goals["Goal2"])
                    prefEditor.putString("Goal3", goals["Goal3"])
                    prefEditor.apply()
                    loadMainActivity(activity, activity.getString(R.string.reg_success))
                    SyncJob.scheduleJob(activity)
                } else {
                    val userInfo = HashMap<String, Any>()
                    userInfo["Burst"] = 0
                    userInfo["TotalSurveys"] = 0
                    userInfo["TotalProofs"] = 0
                    userInfo["StartDate"] = Calendar.getInstance().time
                    val zurichParticipant = participantId.startsWith("z", true)
                    userInfo["SalivaSampling"] = zurichParticipant
                    userInfo["HairSampling"] = zurichParticipant
                    val goals = HashMap<String, Any?>()
                    goals["Goal1"] = null
                    goals["Goal2"] = null
                    goals["Goal3"] = null
                    userInfo["Goals"] = goals
                    docRef.set(userInfo)
                            .addOnSuccessListener { documentReference ->
                                prefEditor.putBoolean(activity.getString(R.string.preg), true)
                                prefEditor.putInt("Burst", 0)
                                prefEditor.putInt("TotalSurveys", 0)
                                prefEditor.putInt("TotalProofs", 0)
                                prefEditor.putBoolean("SalivaSampling", zurichParticipant)
                                prefEditor.putBoolean("HairSampling", zurichParticipant)
                                prefEditor.putString("Goal1", null)
                                prefEditor.putString("Goal2", null)
                                prefEditor.putString("Goal3", null)
                                prefEditor.apply()
                                loadMainActivity(activity, activity.getString(R.string.reg_success))
                                SyncJob.scheduleJob(activity)
                            }
                            .addOnFailureListener { e ->
                                updateUI(activity, activity.getText(R.string.reg_failed) as String)
                            }
                }
            } else {
                Log.w("registerUser", "get failed with ", task.exception)
                updateUI(activity, activity.getText(R.string.reg_failed) as String)
            }
        }
    }

    fun addConfigurationScore(activity: Activity, userInfo: Map<String, Any>) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val pid = sharedPref.getString(activity.getString(R.string.pid), "unknown")
        val db = DBUtils.getDatabase()
        val prefEditor = sharedPref.edit()
        prefEditor.putInt("MeanSetSize", ((userInfo["Calibration"] as Map<String, Any>)["MeanSetSize"] as Double).toInt())
        prefEditor.apply()
        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun retrieveUserGoals(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val localGoal1 = sharedPref.getString("Goal1", "")
        val localGoal2 = sharedPref.getString("Goal2", "")
        val localGoal3 = sharedPref.getString("Goal3", "")
        if (localGoal1.isNullOrEmpty() || localGoal2.isNullOrEmpty() || localGoal3.isNullOrEmpty()) {
            val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")
            val burst = sharedPref.getInt("Burst", 0)
            val db = DBUtils.getDatabase()
            val docRef = db.collection("Users").document(participantId)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null && document.exists()) {
                        val goals = if (burst == 0) (document.data!!["Goals"] as Map<String, String>) else (document.data!!["Burst$burst"] as Map<String, Any>)["Goals"] as Map<String, String>
                        val goal1 = goals["Goal1"]
                        val goal2 = goals["Goal2"]
                        val goal3 = goals["Goal3"]
                        if (goal1 == null || goal2 == null || goal3 == null) {
                            updateUI(context, context.getString(R.string.goal_not_updt))
                        } else {
                            val prefEditor = sharedPref.edit()
                            prefEditor.putString("Goal1", goal1)
                            prefEditor.putString("Goal2", goal2)
                            prefEditor.putString("Goal3", goal3)
                            prefEditor.apply()

                            goalsRetrieved(context)
                        }
                    } else {
                        Log.w("retrieveUserGoals", "No such document")
                        updateUI(context, context.getString(R.string.data_not_found))
                    }
                } else {
                    Log.w("retrieveUserGoals::F", "get failed with ", task.exception)
                    updateUI(context, context.getString(R.string.user_data_failed))
                }
            }
        } else
            goalsRetrieved(context)
    }

    private fun goalsRetrieved(context: Context) {
        updateUI(context, context.getString(R.string.goal_retrieved))
        val intent = Intent(context, LoadingActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        intent.putExtra("addNotification", true)
        context.startActivity(intent)
    }

    fun addBurst(context: Context, timeMap: TimeMap) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = sharedPref.getInt("Burst", 0)
        val totalSurveys = sharedPref.getInt("TotalSurveys", 0)
        val totalProofs = sharedPref.getInt("TotalProofs", 0)

        val prefEditor = sharedPref.edit()
        val json = Gson().toJson(timeMap)
        Log.d("KAKA::SHIFU", "addBurst")
        prefEditor.putInt("StartSurvey", totalSurveys + 1)
        prefEditor.putInt("FirstProof", totalProofs + 1)
        prefEditor.putString(context.getString(R.string.time_map_key), json)
        prefEditor.putInt("PenalityDays", 0)
        prefEditor.putBoolean("ScheduleSurvey", true)
        prefEditor.putLong("LastDay", timeMap.endDate.time - 86400000)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val goals = HashMap<String, String>()
        goals["Goal1"] = sharedPref.getString("Goal1", "")
        goals["Goal2"] = sharedPref.getString("Goal2", "")
        goals["Goal3"] = sharedPref.getString("Goal3", "")

        val burstData = HashMap<String, Any>()
        burstData["StartDate"] = timeMap.startDate
        burstData["EndDate"] = timeMap.endDate
        burstData["SamplingStartDate"] = timeMap.samplingStartDate
        burstData["SamplingEndDate"] = timeMap.samplingEndDate
        burstData["PenalityDays"] = 0
        burstData["SamplingPenalityDays"] = 0
        burstData["StartSurvey"] = totalSurveys + 1
        burstData["EndSurvey"] = totalSurveys
        burstData["TotalSurveys"] = 0
        burstData["Goals"] = goals
        burstData["TotalProofs"] = 0
        burstData["FirstProof"] = totalProofs + 1
        burstData["LastProof"] = totalProofs
        val userInfo = HashMap<String, Any>()
        userInfo["Burst${burst + 1}"] = burstData

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())

        loadMainActivity(context as Activity, context.getString(R.string.scheduled_notifications))
    }

    fun addSurvey(activity: Activity, pid: String, burst: String, surveyId: Int, date: String, surveyTime: Int, newUserResponse: HashMap<String, Any>) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)

        val prefEditor = sharedPref!!.edit()
        prefEditor.putInt("TotalSurveys", surveyId)
        val timeMapObj = Gson().fromJson(sharedPref.getString(activity.getString(R.string.time_map_key), "'"), TimeMap::class.java)
        val dateMap = timeMapObj.timeMap[date]
        dateMap!!.completedSurveys.add(surveyTime)
        val json = Gson().toJson(timeMapObj)
        Log.d("KAKA::SHIFU", "addSurvey")
        prefEditor.putString(activity.getString(R.string.time_map_key), json)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["TotalSurveys"] = surveyId
        val burstData = HashMap<String, Any>()
        burstData["EndSurvey"] = surveyId
        burstData["TotalSurveys"] = surveyId - sharedPref.getInt("StartSurvey", 0) + 1
        userInfo[burst] = burstData

        db.collection("Users").document(pid).set(userInfo, SetOptions.merge())
        db.collection("Users")
                .document(pid)
                .collection("Surveys")
                .document("$surveyId")
                .set(newUserResponse)

        syncData(activity)
    }

    fun updateBurstCompletion(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = sharedPref.getInt("Burst", 0) + 1

        val prefEditor = sharedPref!!.edit()
        prefEditor.putInt("Burst", burst)
        prefEditor.putBoolean("ScheduleSurvey", false)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["Burst"] = burst

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun schedulePenalityJob(context: Context, timeMS: Long) {
        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

        val targetTime = Calendar.getInstance()
        targetTime.timeInMillis = timeMS

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = "Burst${sharedPref.getInt("Burst", 0) + 1}"
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val dayMap = timeMapObj.timeMap[simpleDateFormat.format(targetTime.time)]

        if (dayMap!!.completedSurveys.size < 5) {
            val penalityDays = sharedPref.getInt("PenalityDays", 0)
            if (penalityDays < 3)
                addPenalityDay(context, pid, burst)
        }
    }

    private fun addPenalityDay(context: Context, pid: String, burst: String) {
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))
        dispatcher.cancel(LastDayJob.TAG)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val targetDate = Calendar.getInstance()
        targetDate.time = timeMapObj.endDate

        val weekdayWake = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
        val weekdaySleep = weekdayWake + 720
        val weekendWake = sharedPref.getInt(context.getString(R.string.weekend_wake_key), 480)
        val weekendSleep = weekendWake + 720
        val penalityDays = sharedPref.getInt("PenalityDays", 0) + 1

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

        val dayMap = NotifyJob.scheduleNotificationForDay(context, dispatcher, targetDate, weekdayWake, weekdaySleep, weekendWake, weekendSleep)

        timeMapObj.timeMap[simpleDateFormat.format(targetDate.time)] = dayMap
        targetDate.add(Calendar.DATE, 1)

        LastDayJob.scheduleJob(dispatcher, targetDate.timeInMillis)

        val burstTimeMap = TimeMap(timeMapObj.timeMap, timeMapObj.startDate, targetDate.time, timeMapObj.samplingStartDate, timeMapObj.samplingEndDate)

        val prefEditor = sharedPref.edit()
        val json = Gson().toJson(burstTimeMap)
        prefEditor.putString(context.getString(R.string.time_map_key), json)
        prefEditor.putInt("PenalityDays", penalityDays)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        val burstData = HashMap<String, Any>()
        burstData["PenalityDays"] = penalityDays
        burstData["EndDate"] = targetDate.time
        userInfo[burst] = burstData

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }

    fun addProof(activity: Activity, pid: String, date: String, wakeUpTime: Date, sleepHours: Int, samplingTime: Date, scheduleNotification: Boolean, savedSamplingTime: Int): Int {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val photoId = sharedPref.getInt("TotalProofs", 0) + 1
        val burst = sharedPref.getInt("Burst", 0) + 1

        val prefEditor = sharedPref!!.edit()

        if (scheduleNotification)
            SamplingNotifyJob.scheduleSamplingNotifications(activity, wakeUpTime)

        val timeMapObj = Gson().fromJson(sharedPref.getString(activity.getString(R.string.time_map_key), "'"), TimeMap::class.java)
        val dateMap = timeMapObj.timeMap[date]
        val newDateMap = DayMap(dateMap!!.surveyTimes,
                dateMap.completedSurveys,
                dateMap.completedProofs,
                dateMap.proofTimes,
                wakeUpTime,
                sleepHours)
        if (!scheduleNotification)
            newDateMap.completedProofs.add(savedSamplingTime)

        timeMapObj.timeMap[date] = newDateMap
        val json = Gson().toJson(timeMapObj)
        prefEditor.putString(activity.getString(R.string.time_map_key), json)
        prefEditor.putInt("TotalProofs", photoId)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        userInfo["TotalProofs"] = photoId
        val burstData = HashMap<String, Any>()
        burstData["LastProof"] = photoId
        burstData["TotalProofs"] = photoId - sharedPref.getInt("FirstProof", 0) + 1
        userInfo["Burst$burst"] = burstData

        db.collection("Users").document(pid).set(userInfo, SetOptions.merge())

        val userProof = HashMap<String, Any>()
        userProof["WakeTime"] = wakeUpTime
        userProof["SleepTime"] = sleepHours
        userProof["SamplingTime"] = samplingTime

        db.collection("Users")
                .document(pid)
                .collection("Proofs")
                .document("$photoId")
                .set(userProof)

        return photoId
    }

    fun syncData(context: Context) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")

        val db = DBUtils.getDatabase()
        db.collection("Users").document(participantId).get()

        uploadOfflineProofs(participantId)
    }

    fun addImage(pid: String, proofId: Int, imageFileLocation: String) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val newProof = realm.createObject(SamplingProof::class.java, proofId)
            newProof.imageFileLocation = imageFileLocation
        }
        realm.close()
        uploadOfflineProofs(pid)
    }

    private fun uploadImage(pid: String, proofId: Int, imageFileLocation: String) {
        val mStorageRef = FirebaseStorage.getInstance().reference
        val mProofPhotosStorageReference = mStorageRef.child("HormoneSamplingProof")

        val imageFile = File(imageFileLocation)
        if (imageFile.exists()) {
            val selectedImageUri = Uri.fromFile(imageFile)
            val photoRef = mProofPhotosStorageReference.child("$pid/${proofId}_${selectedImageUri.lastPathSegment}")
            photoRef.putFile(selectedImageUri)
                    .addOnSuccessListener {
                        val downloadUrl = it.downloadUrl.toString()
                        updateURI(pid, proofId, downloadUrl)
                        imageFile.delete()
                    }
        } else {
            updateURI(pid, proofId, "File Not Found")
        }
    }

    private fun updateURI(pid: String, proofId: Int, proofURI: String) {
        val db = DBUtils.getDatabase()
        val userProof = HashMap<String, Any>()
        userProof["URI"] = proofURI
        db.collection("Users")
                .document(pid)
                .collection("Proofs")
                .document("$proofId")
                .set(userProof, SetOptions.merge())
                .addOnSuccessListener {
                    updateUploadDate(proofId)
                }
    }

    private fun updateUploadDate(proofId: Int) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            val samplingProof = realm.where(SamplingProof::class.java).equalTo("proofID", proofId).findFirst()
            samplingProof!!.uploadDate = Calendar.getInstance().time
        }
        realm.close()
    }

    private fun uploadOfflineProofs(pid: String) {
        val offlineProofs = getOfflineProofs()
        if (offlineProofs != null) {
            for (offlineProof in offlineProofs)
                uploadImage(pid, offlineProof.proofID, offlineProof.imageFileLocation)
        }
    }

    private fun getOfflineProofs(): RealmResults<SamplingProof>? {
        val realm = Realm.getDefaultInstance()

        return realm.where(SamplingProof::class.java).isNull("uploadDate").findAll()
    }

    fun scheduleSamplingPenaltyJob(context: Context, timeMS: Long) {
        val targetTime = Calendar.getInstance()
        targetTime.timeInMillis = timeMS

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pid = sharedPref.getString(context.getString(R.string.pid), "unknown")
        val burst = "Burst${sharedPref.getInt("Burst", 0) + 1}"
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val targetDate = simpleDateFormat.format(targetTime.time)
        val dayMap = timeMapObj.timeMap[targetDate]
        val samplingPenalityDays = sharedPref.getInt("SamplingPenalityDays", 0)
        val completedProofs = dayMap!!.completedProofs.size

        val endSamplingDate = Calendar.getInstance()
        endSamplingDate.time = timeMapObj.samplingEndDate
        endSamplingDate.add(Calendar.DATE, -1)

        if (completedProofs < 3 && samplingPenalityDays < 1)
            addSamplingPenalityDay(context, dispatcher, pid, burst)
        else if (timeMS < endSamplingDate.timeInMillis)
            SamplingNotifyJob.scheduleJob(dispatcher, Calendar.getInstance().timeInMillis, context.getString(R.string.remainder_a))
    }

    private fun addSamplingPenalityDay(context: Context, dispatcher: FirebaseJobDispatcher, pid: String, burst: String) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val timeMapObj = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        val targetDate = Calendar.getInstance()

        val samplingPenalityDays = sharedPref.getInt("SamplingPenalityDays", 0) + 1

        SamplingNotifyJob.scheduleJob(dispatcher, targetDate.timeInMillis, context.getString(R.string.remainder_a))

        val samplingEndDate = Calendar.getInstance()
        samplingEndDate.time = timeMapObj.samplingEndDate
        samplingEndDate.add(Calendar.DATE, 1)
        val burstTimeMap = TimeMap(timeMapObj.timeMap, timeMapObj.startDate, timeMapObj.endDate, timeMapObj.samplingStartDate, samplingEndDate.time)

        val prefEditor = sharedPref.edit()
        val json = Gson().toJson(burstTimeMap)
        prefEditor.putString(context.getString(R.string.time_map_key), json)
        prefEditor.putInt("SamplingPenalityDays", samplingPenalityDays)
        prefEditor.apply()

        val db = DBUtils.getDatabase()

        val userInfo = HashMap<String, Any>()
        val burstData = HashMap<String, Any>()
        burstData["SamplingPenalityDays"] = samplingPenalityDays
        burstData["SamplingEndDate"] = samplingEndDate.time
        userInfo[burst] = burstData

        db.collection("Users")
                .document(pid)
                .set(userInfo, SetOptions.merge())
    }
}