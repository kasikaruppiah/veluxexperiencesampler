package edu.northeastern.ledlab.veluxexperiencesampler

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import io.realm.Realm


/**
 * Created by ledlab on 9/1/17.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)

        registerActivityLifecycleCallbacks(AppLifeCycleCallbacks())
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(base!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
    }
}

class AppLifeCycleCallbacks : Application.ActivityLifecycleCallbacks {
    private var numStarted = 0

    override fun onActivityPaused(activity: Activity?) {
        Log.d("KAKA", "onActivityPaused")
    }

    override fun onActivityResumed(activity: Activity?) {
        Log.d("KAKA", "onActivityResumed")
    }

    override fun onActivityStarted(activity: Activity?) {
        Log.d("KAKA", "onActivityStarted")
        if (numStarted == 0) {
            Log.d("KAKA", "FOREGROUND")
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
            val prefEditor = sharedPref.edit()
            prefEditor.putBoolean("APPOPENEDAFTERNOTIFICATION", true)
            prefEditor.apply()
        }
        numStarted++
    }

    override fun onActivityDestroyed(activity: Activity?) {
        Log.d("KAKA", "onActivityDestroyed")
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        Log.d("KAKA", "onActivitySaveInstanceState")
    }

    override fun onActivityStopped(activity: Activity?) {
        Log.d("KAKA", "onActivityStopped")
        numStarted--
        if (numStarted == 0)
            Log.d("KAKA", "BACKGROUND")
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        Log.d("KAKA", "onActivityCreated")
    }
}
