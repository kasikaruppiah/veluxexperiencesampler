package edu.northeastern.ledlab.veluxexperiencesampler

import android.Manifest
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_hormone_sampling.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class HormoneSamplingActivity : BaseActivity() {
    private val RC_PHOTO_CAPTURE = 2
    private val REQUEST_EXTERNAL_STORAGE = 3

    private var pid: String? = null
    private var imageFileLocation: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hormone_sampling)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        pid = sharedPref.getString(this.getString(R.string.pid), "unknown")

        val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

        val wakeUpCalendar = Calendar.getInstance()

        val samplingCalendar = Calendar.getInstance()
        val currentDate = simpleDateFormat.format(samplingCalendar.time)

        val samplingHour = samplingCalendar.get(Calendar.HOUR_OF_DAY)
        val samplingMinute = samplingCalendar.get(Calendar.MINUTE)
        var scheduleNotification = false

        if (intent.getSerializableExtra("WakeUpTime") != null) {
            wakeup_time_label.visibility = View.GONE
            wakeup_time.visibility = View.GONE
            wakeUpCalendar.time = intent.getSerializableExtra("WakeUpTime") as Date
        } else {
            wakeup_time.text = String.format(getString(R.string.hormoneTimeChange), samplingHour, samplingMinute)
            scheduleNotification = true
        }

        timePicker.setIs24HourView(true)
        var sleepHours = 480
        if (intent.getSerializableExtra("SleepHours") != null) {
            timePicker_label.visibility = View.GONE
            timePicker.visibility = View.GONE
            sleepHours = intent.getSerializableExtra("SleepHours") as Int
        } else {
            val sleepHour = sleepHours / 60
            val sleepMinute = sleepHours % 60
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                timePicker.hour = sleepHour
                timePicker.minute = sleepMinute
            } else {
                timePicker.currentHour = sleepHour
                timePicker.currentMinute = sleepMinute
            }
        }

        wakeup_time.setOnClickListener {
            val time = wakeup_time.text.split(" ")[0].split(":")
            val timePickerDialog = TimePickerDialog(this, R.style.AppAlertDialog,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        wakeUpCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        wakeUpCalendar.set(Calendar.MINUTE, minute)
                        wakeup_time.text = String.format(getString(R.string.hormoneTimeChange), hourOfDay, minute)
                    },
                    time[0].toInt(), time[1].toInt(), true)
            timePickerDialog.show()
        }

        timePicker.setOnTimeChangedListener { view, hourOfDay, minute ->
            sleepHours = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                timePicker.hour * 60 + timePicker.minute
            } else {
                timePicker.currentHour * 60 + timePicker.currentMinute
            }
        }

        sampling_time.text = String.format(getString(R.string.hormoneTimeChange), samplingHour, samplingMinute)
        sampling_time.setOnClickListener {
            val time = sampling_time.text.split(" ")[0].split(":")
            val timePickerDialog = TimePickerDialog(this, R.style.AppAlertDialog,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        samplingCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        samplingCalendar.set(Calendar.MINUTE, minute)
                        sampling_time.text = String.format(getString(R.string.hormoneTimeChange), hourOfDay, minute)
                    },
                    time[0].toInt(), time[1].toInt(), true)
            timePickerDialog.show()
        }

        image_view.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission
                                .WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    takePhoto()
                else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        Toast.makeText(this, getString(R.string.writePermission), Toast.LENGTH_SHORT).show()
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_EXTERNAL_STORAGE)
                }
            } else
                takePhoto()
        }

        val samplingTime = intent.getIntExtra("time", 0)

        upld_image.isEnabled = false
        upld_image.setOnClickListener {
            val proofId = DBUtils.addProof(this, pid!!, currentDate, wakeUpCalendar.time, sleepHours, samplingCalendar.time, scheduleNotification, samplingTime)

            DBUtils.addImage(pid!!, proofId, imageFileLocation!!)

            finish()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE)
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                takePhoto()
            else
                Toast.makeText(this, getString(R.string.writeDenied), Toast.LENGTH_SHORT).show()
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_PHOTO_CAPTURE && resultCode == Activity.RESULT_OK) {
            Glide.with(this).load(imageFileLocation).into(image_view)
            upld_image.isEnabled = true
        }
    }

    private fun takePhoto() {
        val cameraIntent = Intent()
        cameraIntent.action = MediaStore.ACTION_IMAGE_CAPTURE
        var image: File? = null
        try {
            image = createImageFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", image))
        startActivityForResult(cameraIntent, RC_PHOTO_CAPTURE)
    }

    private fun createImageFile(): File {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val fileName = "${timestamp}_"
        val storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(fileName, ".jpg", storageDirectory)
        imageFileLocation = image.absolutePath

        return image
    }
}
