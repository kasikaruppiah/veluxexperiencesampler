package edu.northeastern.ledlab.veluxexperiencesampler

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.view.WindowManager
import android.widget.*
import kotlinx.android.synthetic.main.activity_dot_memory_task.*
import java.util.*


class DotMemoryTask : BaseActivity() {
    private val gridSize = 6

    private val buttonTags = ArrayList<String>()
    private var trialValidTags = ArrayList<String>()
    private var trialUserTags = ArrayList<String>()
    private val assessmentSizes = ArrayList<Int>()
    private val trialSizes = ArrayList<Int>()
    private val trialDots = ArrayList<Int>()
    private val trialTimes = ArrayList<Long>()
    private val trialEuclideanScores = ArrayList<Double>()
    private val dotCorrectness = ArrayList<Int>()
    private val dotTimes = ArrayList<Long>()
    private val dotEuclideanScores = ArrayList<Double>()
    private val tempDotTimes = ArrayList<Long>()
    private var dotStartTime = Calendar.getInstance().timeInMillis

    private val handler = Handler()
    private var currentAssessment = 1
    private var totalAssessment = 20
    private var currentTrial = 0
    private var totalTrial = 2
    private var assessmentScore = 0
    private var trialSpeed = 1
    private var currentSetSize = 3
    private var totalSetSize = gridSize * gridSize
    private var currentPracticeTrial = 0
    private var totalPracticeTrials = 0
    private var nby2 = 0
    private var endTrialTime = Calendar.getInstance().timeInMillis
    private var meanSetSize = 0.0
    private var log = "{"

    private var countDownTimer: CountDownTimer? = null
    private var countDownTimerPaused = false
    private var gameInProgress = false
    private var validatingTrial = false
    private var breakSegment = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dot_memory_task)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        val dotIntent = intent
        totalAssessment = dotIntent.getIntExtra("assessment", 20)
        nby2 = totalAssessment / 2
        currentSetSize = dotIntent.getIntExtra("setsize", 3)
        totalPracticeTrials = dotIntent.getIntExtra("practicetrials", 0)

        createGrid()
    }

    override fun onBackPressed() {
        pause()
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setCancelable(false)
        alertDialog.setMessage(getString(R.string.back_pressed))
        alertDialog.setPositiveButton(getString(R.string.ok)) { p0, p1 ->
            if (countDownTimer != null)
                countDownTimer!!.cancel()
            finish()
        }
        alertDialog.setNegativeButton(getString(R.string.cancel)) { p0, p1 -> resume() }
        alertDialog.show()
    }

    private fun pause() {
        if (gameInProgress) {
            handler.removeCallbacksAndMessages(null)
            if (countDownTimer != null) {
                countDownTimerPaused = true
                countDownTimer!!.cancel()
                countDownTimer = null
            }
        }
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    override fun onPause() {
        super.onPause()
        pause()
    }

    private fun resume() {
        if (gameInProgress) {
            if (!breakSegment) {
                relative_layout.findViewWithTag<ImageView>("gridmask").visibility = View.INVISIBLE
                val button = relative_layout.findViewWithTag<Button>("ddisplay")
                button.text = getString(R.string.resume)
                button.isClickable = true
                button.setOnClickListener { v ->
                    v.visibility = View.INVISIBLE
                    if (countDownTimerPaused) {
                        countDownTimerPaused = false
                        val dtimer = relative_layout.findViewWithTag<ProgressBar>("dtimer")
                        countDownTimer = Timer((dtimer.max - dtimer.progress).toLong(), 250)
                        countDownTimer!!.start()
                    } else {
                        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        if (validatingTrial) {
                            trialValidTags
                                    .asSequence()
                                    .map { relative_layout.findViewWithTag<Button>(it) }
                                    .forEach { it.setBackgroundColor(Color.WHITE) }
                            handler.postDelayed(this::validateTrial, 100)
                        } else {
                            trialValidTags
                                    .asSequence()
                                    .map { relative_layout.findViewWithTag<Button>(it) }
                                    .forEach { it.setBackgroundColor(Color.WHITE) }
                            if (currentAssessment == 1 && currentTrial == 0 && currentPracticeTrial <= totalPracticeTrials)
                                currentPracticeTrial -= 1
                            else if (currentTrial > 0)
                                currentTrial -= 1
                            handler.postDelayed(this::newTrial, 1000)
                        }
                    }
                }
                button.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        resume()
    }

    private fun getScreenSize(): Int {
        val configuration = resources.configuration
        val width = configuration.screenWidthDp
        val height = configuration.screenHeightDp

        return Math.min(Math.min(Math.min(height, width), 320) + 32, height)
    }

    private fun createGrid() {
        val dm = resources.displayMetrics
        val xdpi = dm.xdpi
        val ydpi = dm.ydpi
        val dpi = Math.sqrt((xdpi * ydpi).toDouble())
        val scale = dpi / 160
        val size = getScreenSize()
        val gameSize = (size * scale + 0.5f).toInt()
        val gridsSize = ((size - 32) * scale + 0.5f).toInt()
        val padding = (4 * scale + 0.5f).toInt()
        val progressPadding = padding * 2

        val outerLinearLayout = LinearLayout(this)
        val outerParams = RelativeLayout.LayoutParams(gridsSize, gameSize)
        outerParams.addRule(RelativeLayout.CENTER_IN_PARENT)
        outerLinearLayout.orientation = LinearLayout.VERTICAL
        outerLinearLayout.layoutParams = outerParams

        val gridRelativeLayout = RelativeLayout(this)
        gridRelativeLayout.layoutParams = LinearLayout.LayoutParams(gridsSize, gridsSize)

        val gridLinearLayout = LinearLayout(this)
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> gridLinearLayout.background = resources.getDrawable(R.drawable.border, theme)
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> gridLinearLayout.background = resources.getDrawable(R.drawable.border)
            else -> gridLinearLayout.setBackgroundDrawable(resources.getDrawable(R.drawable.border))
        }

        gridLinearLayout.orientation = LinearLayout.VERTICAL
        gridLinearLayout.weightSum = gridSize.toFloat()
        gridLinearLayout.setPadding(padding, padding, padding, padding)

        val gridParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        gridParams.addRule(RelativeLayout.CENTER_IN_PARENT)
        gridLinearLayout.layoutParams = gridParams

        for (i in 0 until gridSize) {
            val innerLinearLayout = LinearLayout(this)
            innerLinearLayout.orientation = LinearLayout.HORIZONTAL
            innerLinearLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1F)
            innerLinearLayout.weightSum = gridSize.toFloat()
            for (j in 0 until gridSize) {
                val button = Button(this, null, R.style.Widget_AppCompat_Button_Borderless)
                val tag = "grid_button_$i$j"
                buttonTags.add(tag)
                button.tag = tag
                button.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1F)
                button.setBackgroundColor(Color.WHITE)
                button.setOnClickListener { v ->
                    trialUserTags.add(v.tag.toString())
                    v.setBackgroundColor(Color.BLACK)
                    val dotEndTime = Calendar.getInstance().timeInMillis
                    val dotTime = dotEndTime - dotStartTime
                    tempDotTimes.add(dotTime)
                    window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    if (trialUserTags.size == currentSetSize) {
                        countDownTimer!!.cancel()
                        countDownTimer = null
                        endTrialTime = Calendar.getInstance().timeInMillis
                        validatingTrial = true
                        handler.postDelayed({
                            v.setBackgroundColor(Color.WHITE)
                            validateTrial()
                        }, 100)
                    } else
                        handler.postDelayed({
                            v.setBackgroundColor(Color.WHITE)
                            dotStartTime = Calendar.getInstance().timeInMillis
                            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        }, 100)
                }
                innerLinearLayout.addView(button)
            }
            gridLinearLayout.addView(innerLinearLayout)
        }
        gridRelativeLayout.addView(gridLinearLayout)

        val maskParams = RelativeLayout.LayoutParams(gridsSize, gridsSize)
        maskParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)

        val maskImage = ImageView(this)
        maskImage.tag = "gridmask"
        maskImage.layoutParams = maskParams
        maskImage.scaleType = ImageView.ScaleType.FIT_XY
        maskImage.setImageResource(R.drawable.black_and_white_grainy_dot)
        maskImage.visibility = View.INVISIBLE
        gridRelativeLayout.addView(maskImage)

        val button = Button(this)
        button.tag = "ddisplay"
        button.setBackgroundColor(Color.WHITE)
        button.text = getString(R.string.ready)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            button.setTextAppearance(android.R.style.TextAppearance_Large)
        else
            button.setTextAppearance(this, android.R.style.TextAppearance_Large)
        button.layoutParams = maskParams
        button.setOnClickListener { v ->
            gameInProgress = true
            v.visibility = View.INVISIBLE
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            handler.postDelayed(this::newTrial, 1000)
        }
        gridRelativeLayout.addView(button)

        outerLinearLayout.addView(gridRelativeLayout)

        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal)
        progressBar.tag = "dtimer"
        progressBar.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        progressBar.setPadding(progressPadding, progressPadding, progressPadding, progressPadding)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            progressBar.progressTintList = ColorStateList.valueOf(Color.BLACK)
        else
            progressBar.progressDrawable.setColorFilter(Color.BLACK, android.graphics.PorterDuff.Mode.SRC_IN)
        outerLinearLayout.addView(progressBar)

        relative_layout.addView(outerLinearLayout)
    }

    private fun checkAdjacent(previous: String, current: String): Boolean {
        val previousGrid = previous.takeLast(2).toInt()
        val currentGrid = current.takeLast(2).toInt()

        return Math.abs(currentGrid - previousGrid) in listOf(1, 9, 10, 11)
    }

    private fun getNButtons(n: Int): List<String> {
        buttonTags.shuffle()
        val randomTags = ArrayList<String>()
        val random = Random()
        val size = buttonTags.size
        var index = 0
        for (i in 0 until n) {
            index = random.nextInt(size)
            if (i == 1)
                while (buttonTags[index] == randomTags[i - 1] || checkAdjacent(randomTags[i - 1], buttonTags[index]))
                    index = random.nextInt(size)
            else if (i > 1)
                while (buttonTags[index] == randomTags[i - 1] || buttonTags[index] == randomTags[i - 2] || checkAdjacent(randomTags[i - 1], buttonTags[index]))
                    index = random.nextInt(size)
            randomTags.add(buttonTags[index])
        }
        return randomTags
    }

    private fun newTrial() {
        if (currentTrial == totalTrial) {
            assessmentSizes.add(currentSetSize)

            if (assessmentScore > currentSetSize)
                currentSetSize += 1
            else if (assessmentScore < currentSetSize)
                currentSetSize -= 1
            if (currentSetSize > totalSetSize)
                currentSetSize = totalSetSize
            else if (currentSetSize < 1)
                currentSetSize = 1

            assessmentScore = 0
            currentTrial = 0
            currentAssessment += 1
        }
        val button = relative_layout.findViewWithTag<Button>("ddisplay")
        if (currentPracticeTrial < totalPracticeTrials || currentAssessment <= totalAssessment) {
            trialUserTags = ArrayList()
            if (currentPracticeTrial < totalPracticeTrials) {
                trialSpeed = totalPracticeTrials - currentPracticeTrial
                currentPracticeTrial += 1
                button.text = String.format(getString(R.string.practice_trial), currentPracticeTrial)
            } else {
                currentTrial += 1
                button.text = if (currentTrial == 1 && currentAssessment == 1) getString(R.string.start_game) else getString(R.string.new_trial)
            }
            trialValidTags = ArrayList(getNButtons(currentSetSize))
            tempDotTimes.clear()
            button.isClickable = false
            button.visibility = View.VISIBLE
            handler.postDelayed({
                button.visibility = View.INVISIBLE
                button.isClickable = true
                displayTrialGrid()
            }, 1000)
        } else {
            gameInProgress = false
            log += "}"
            meanSetSize = assessmentSizes.subList(nby2, currentAssessment - 1).sum() / (currentAssessment - 1 - nby2).toDouble()
            val meanSetSize40 = assessmentSizes.sum() / assessmentSizes.size.toDouble()
            val meanDots = trialDots.sum() / trialDots.size.toDouble()
            button.text = String.format(getString(R.string.mean_set_size), meanSetSize)
            val score = HashMap<String, Any>()

            score["Log"] = log
            score["TrialSizes"] = trialSizes.toString()
            score["TrialDots"] = trialDots.toString()
            score["TrialEuclideanScores"] = trialEuclideanScores.toString()
            score["TrialTimes"] = trialTimes.toString()
            score["DotsCorrect"] = dotCorrectness.toString()
            score["DotsEuclideanScores"] = dotEuclideanScores.toString()
            score["DotTimes"] = dotTimes.toString()
            score["MeanSetSize40"] = meanSetSize40
            score["MeanSetSize"] = meanSetSize
            score["MeanDotsCorrect40"] = meanDots
            score["EuclideanScore"] = trialEuclideanScores.sum()
            score["Time40"] = trialTimes.sum()
            val userInfo = HashMap<String, Any>()
            userInfo["Calibration"] = score
            button.setOnClickListener(null)
            button.setOnLongClickListener {
                DBUtils.addConfigurationScore(this, userInfo)
                finish()
                true
            }
            button.visibility = View.VISIBLE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    private fun displayTrialGrid() {
        var timer = 500L

        for (tag in trialValidTags) {
            val btn = relative_layout.findViewWithTag<Button>(tag)
            handler.postDelayed({
                btn.setBackgroundColor(Color.BLACK)
            }, timer)
            timer += 1500 * trialSpeed
            handler.postDelayed({
                btn.setBackgroundColor(Color.WHITE)
            }, timer)
            timer += 150 * trialSpeed
        }
        handler.postDelayed({
            relative_layout.findViewWithTag<ImageView>("gridmask").visibility = View.VISIBLE
        }, timer)
        timer += 1000
        handler.postDelayed({
            relative_layout.findViewWithTag<ImageView>("gridmask").visibility = View.INVISIBLE
            val dtimer = relative_layout.findViewWithTag<ProgressBar>("dtimer")
            dtimer.max = currentSetSize * 1750 * 2 * trialSpeed
            countDownTimer = Timer(dtimer.max.toLong(), 250)
            countDownTimer!!.start()
            dotStartTime = Calendar.getInstance().timeInMillis
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }, timer)
    }

    private fun euclideanDistance(start: Int, end: Int): Double {
        val x1 = start / 10
        val y1 = start % 10
        val x2 = end / 10
        val y2 = end % 10
        val xDiff = (x2 - x1).toDouble()
        val yDiff = (y2 - y1).toDouble()

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff)
    }

    private fun validateTrial() {
        var timer = 0L
        val interval = (2000 / currentSetSize).toLong()

        var time = tempDotTimes.sum()
        if (trialUserTags.size < currentSetSize)
            time += endTrialTime - dotStartTime

        var tempLog = "<"
        val tempDotCorrectness = ArrayList<Int>()
        val tempDotEuclideanScores = ArrayList<Double>()
        var trialScore = 0

        for (i in 0 until currentSetSize) {
            if (i > 0)
                tempLog += ","
            val btn = relative_layout.findViewWithTag<Button>(trialValidTags[i])
            tempLog += trialValidTags[i].takeLast(2) + ":"
            var drawable = R.drawable.ic_close_black_24dp
            if (i < trialUserTags.size) {
                tempLog += trialUserTags[i].takeLast(2)
                if (trialUserTags[i] == trialValidTags[i]) {
                    drawable = R.drawable.ic_check_black_24dp
                    tempDotEuclideanScores.add(0.0)
                    tempDotCorrectness.add(1)
                    trialScore += 1
                } else {
                    tempDotEuclideanScores.add(euclideanDistance(trialUserTags[i].takeLast(2).toInt(), trialValidTags[i].takeLast(2).toInt()))
                    tempDotCorrectness.add(0)
                }
            } else {
                tempDotEuclideanScores.add(euclideanDistance(0, gridSize * 10 + gridSize))
                tempDotCorrectness.add(-1)
                tempLog += "N"
                tempDotTimes.add(0L)
            }

            handler.postDelayed({
                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> btn.background = resources.getDrawable(drawable, theme)
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> btn.background = resources.getDrawable(drawable)
                    else -> btn.setBackgroundDrawable(resources.getDrawable(drawable))
                }
            }, timer)
            timer += interval
            handler.postDelayed({
                btn.setBackgroundColor(Color.WHITE)
            }, timer)
            timer += 100
        }
        tempLog += ">"
        val trialEuclideanScore = tempDotEuclideanScores.takeLast(currentSetSize).sum() / currentSetSize
        assessmentScore += trialScore

        handler.postDelayed({
            validatingTrial = false
            if (currentAssessment > 1 || (currentAssessment == 1 && currentTrial > 0)) {
                if (currentTrial == 1)
                    log += "["
                log += tempLog
                if (currentTrial == totalTrial)
                    log += "]"
                trialSizes.add(currentSetSize)
                trialTimes.add(time)
                trialDots.add(trialScore)
                trialEuclideanScores.add(trialEuclideanScore)
                dotEuclideanScores.addAll(tempDotEuclideanScores)
                dotCorrectness.addAll(tempDotCorrectness)
                dotTimes.addAll(tempDotTimes)
            }
            relative_layout.findViewWithTag<ProgressBar>("dtimer").progress = 0
            if ((totalPracticeTrials != 0 && currentPracticeTrial == totalPracticeTrials && currentTrial == 0 && currentAssessment == 1) || (currentTrial == 2 && currentAssessment == totalAssessment / 2)) {
                breakSegment = true
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                val button = relative_layout.findViewWithTag<Button>("ddisplay")
                button.text = if (currentTrial == 0 && currentAssessment == 1) getString(R.string.break_practice) else getString(R.string.break_segment)
                button.isClickable = true
                button.setOnClickListener { v ->
                    breakSegment = false
                    v.visibility = View.INVISIBLE
                    window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    handler.postDelayed(this::newTrial, 1000)
                }
                button.visibility = View.VISIBLE
            } else
                newTrial()
        }, timer)
    }

    inner class Timer(millisInFuture: Long, countDownInterval: Long) : CountDownTimer(millisInFuture, countDownInterval) {
        override fun onTick(millisUntilFinished: Long) {
            val dtimer = relative_layout.findViewWithTag<ProgressBar>("dtimer")
            dtimer.progress = (dtimer.max - millisUntilFinished).toInt()
        }

        override fun onFinish() {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            val dtimer = relative_layout.findViewWithTag<ProgressBar>("dtimer")
            dtimer.progress = dtimer.max
            handler.postDelayed({
                endTrialTime = Calendar.getInstance().timeInMillis
                validatingTrial = true
                validateTrial()
            }, 100)
            countDownTimer = null
        }
    }
}