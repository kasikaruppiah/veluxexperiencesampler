package edu.northeastern.ledlab.veluxexperiencesampler

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_survey.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by ledlab on 8/15/17.
 */
class SurveyFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val surveyMenu = inflater!!.inflate(R.layout.fragment_survey, container, false)
        val button = surveyMenu.findViewById<Button>(R.id.survey_button)
        val imageButton = surveyMenu.findViewById<Button>(R.id.image_button)
        refreshTab(button, imageButton)
        return surveyMenu
    }

    override fun onResume() {
        super.onResume()
        refreshTab(survey_button, image_button)
    }

    private fun refreshTab(button: Button, imageButton: Button) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val sampling = sharedPref.getBoolean("SalivaSampling", false)
        val meanSetSize = sharedPref.getInt("MeanSetSize", -1)
        val burst = sharedPref.getInt("Burst", 0) + 1
        val scheduled = sharedPref.getBoolean("ScheduleSurvey", false)
        val timeMap = Gson().fromJson(sharedPref.getString(context.getString(R.string.time_map_key), ""), TimeMap::class.java)
        imageButton.visibility = View.GONE

        if (meanSetSize == -1) {
            button.text = getString(R.string.dot_memory_task)
            button.setOnClickListener({ view ->
                kotlin.run {
                    val intent = Intent(context, DotMemoryConfiguration::class.java)
                    startActivity(intent)
                }
            })
        } else {
            if (scheduled) {
                button.text = getString(R.string.sur)
                button.setOnClickListener({ view ->
                    kotlin.run {
                        val surveyCheck = timeCheck(timeMap)
                        if (surveyCheck.check) {
                            val intent = Intent(context, QuestionnaireActivity::class.java)
                            intent.putExtra("date", surveyCheck.date)
                            intent.putExtra("time", surveyCheck.time)
                            intent.putExtra("dailyCount", surveyCheck.dailyCount)
                            startActivity(intent)
                        } else {
                            Toast.makeText(context, getString(R.string.try_later), Toast.LENGTH_SHORT).show()
                        }
                    }
                })
                val samplingUploadCheck = samplingCheck(burst, sampling, timeMap)
                Log.d("KAKA::SAMPLING CHECK", samplingUploadCheck.toString())
                if (samplingUploadCheck.check) {
                    val dayMap = timeMap.timeMap[samplingUploadCheck.date]
                    imageButton.visibility = View.VISIBLE
                    imageButton.text = getString(R.string.upld_img)
                    imageButton.setOnClickListener { view ->
                        kotlin.run {
                            val intent = Intent(context, HormoneSamplingActivity::class.java)
                            intent.putExtra("WakeUpTime", dayMap!!.wakeUpTime)
                            intent.putExtra("SleepHours", dayMap.sleepHours)
                            intent.putExtra("time", samplingUploadCheck.time)
                            startActivity(intent)
                        }
                    }
                }
            } else {
                button.text = getString(R.string.schedule)
                if (burst < 6) {
                    button.setOnClickListener({ view ->
                        kotlin.run {
                            val alertDialog = AlertDialog.Builder(context)
                            alertDialog.setCancelable(false)
                            alertDialog.setMessage(String.format(context.getString(R.string.schedule_survey), burst))
                            alertDialog.setPositiveButton(getString(R.string.ok)) { p0, p1 ->
                                kotlin.run {
                                    var intent = Intent(context, GoalActivity::class.java)
                                    if (burst == 1) {
                                        intent = Intent(context, LoadingActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.putExtra("scheduleSurvey", true)
                                    }
                                    startActivity(intent)
                                }
                            }
                            alertDialog.setNegativeButton(getString(R.string.cancel)) { p0, p1 -> }
                            alertDialog.show()

                        }
                    })
                } else {
                    button.isEnabled = false
                }
            }
        }
    }

    private fun timeCheck(timeMapObj: TimeMap): SurveyCheck {
        Log.d("KAKA::timeCheck", timeMapObj.toString())
        val calendar = Calendar.getInstance()
        val currDate = calendar.time
        val startDate = timeMapObj.startDate
        val endDate = timeMapObj.endDate
        if ((currDate.after(startDate) or (currDate == startDate)) and currDate.before(endDate)) {
            val currTime = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE)
            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
            val date = simpleDateFormat.format(Date())
            val dayMap = timeMapObj.timeMap[date]
            for (time in dayMap!!.surveyTimes) {
                if (time <= currTime && currTime <= time + 20 && !dayMap.completedSurveys.contains(time))
                    return SurveyCheck(true, date, time, dayMap.completedSurveys.size)
            }
        }

        return SurveyCheck(false, null, null, 0)
    }

    private fun samplingCheck(burst: Int, sampling: Boolean, timeMapObj: TimeMap): SurveyCheck {
        if (arrayListOf(1, 3, 5).contains(burst) and sampling) {
            val calendar = Calendar.getInstance()
            val currDate = calendar.time
            val startDate = timeMapObj.samplingStartDate
            val endDate = timeMapObj.samplingEndDate
            if ((currDate.after(startDate) or (currDate == startDate)) and currDate.before(endDate)) {
                val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")
                val date = simpleDateFormat.format(currDate)
                val dayMap = timeMapObj.timeMap[date]

                val currTime = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE)
                if (dayMap!!.proofTimes != null) {
                    for (time in dayMap.proofTimes!!) {
                        if (time <= currTime && currTime <= time + 150 && !dayMap.completedProofs.contains(time))
                            return SurveyCheck(true, date, time)
                    }
                } else
                    return SurveyCheck(true, date, null)
            }
        }

        return SurveyCheck(false, null, null)
    }
}