package edu.northeastern.ledlab.veluxexperiencesampler

import android.util.Log
import com.firebase.jobdispatcher.*
import java.util.concurrent.TimeUnit
import kotlin.math.max

class LastDayJob : JobService() {
    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "LastDayJob::JobService::onStartJob")

        DBUtils.updateBurstCompletion(this)

        return false
    }

    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "LastDayJob::JobService::onStopJob")

        return true
    }

    companion object {
        const val TAG = "last_day_job"

        fun scheduleJob(dispatcher: FirebaseJobDispatcher, timeInMillis: Long) {
            val timeInSeconds = TimeUnit.MILLISECONDS.toSeconds(max(60000L, timeInMillis - System.currentTimeMillis())).toInt()

            val myJob = dispatcher.newJobBuilder()
                    .setService(LastDayJob::class.java)
                    .setTag(LastDayJob.TAG)
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .build()

            dispatcher.mustSchedule(myJob)
        }
    }
}