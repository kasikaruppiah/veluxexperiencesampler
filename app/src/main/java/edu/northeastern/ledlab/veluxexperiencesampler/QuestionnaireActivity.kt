package edu.northeastern.ledlab.veluxexperiencesampler

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.widget.TextViewCompat
import android.support.v7.app.AlertDialog
import android.text.*
import android.text.style.StyleSpan
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.android.synthetic.main.activity_questionnaire.*
import java.io.InputStreamReader
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger


/**
 * Created by ledlab on 8/16/17.
 */
class QuestionnaireActivity : BaseActivity() {

    private lateinit var survey: Questionnaire
    private lateinit var currentQuestion: Question
    private val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    private val userResponse = HashMap<String, String>()
    private var sharedPref: SharedPreferences? = null
    private var goal1: String = ""
    private var goal2: String = ""
    private var goal3: String = ""
    private var meanSetSize: Int = 3
    private var surveyId: Int = 0
    private var burst: String = "Burst"
    private var newUserResponse = HashMap<String, Any>()
    private var participantId = ""
    private var startTime = Calendar.getInstance()
    private var endTime = Calendar.getInstance()
    private var date = ""
    private var surveyTime = 0
    private var dailyCount = 0
    private var dotMemoryFlag = 0
    private var responses = ArrayList<String>()
    private var multicheckResponses = HashMap<String, ArrayList<String>>()
    private var padding: Int = (4 + 0.5f).toInt()
    private var lastDayCheck = false
    private var sampling = ArrayList<String>()

    init {
        params.setMargins(16, 16, 16, 16)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questionnaire)

        val dm = resources.displayMetrics
        val xdpi = dm.xdpi
        val ydpi = dm.ydpi
        val dpi = Math.sqrt((xdpi * ydpi).toDouble())
        val scale = dpi / 160
        padding = (scale + 0.5f).toInt()

        date = intent.getStringExtra("date")
        surveyTime = intent.getIntExtra("time", 0)
        dailyCount = intent.getIntExtra("dailyCount", 0)

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        meanSetSize = sharedPref!!.getInt("MeanSetSize", 3)
        if (meanSetSize < 3)
            meanSetSize = 3
        participantId = sharedPref!!.getString(this.getString(R.string.pid), "unknown")
        goal1 = sharedPref!!.getString("Goal1", "")
        goal2 = sharedPref!!.getString("Goal2", "")
        goal3 = sharedPref!!.getString("Goal3", "")
        surveyId = sharedPref!!.getInt("TotalSurveys", 0)
        surveyId += 1
        val burstID = sharedPref!!.getInt("Burst", 0) + 1
        burst += burstID
        Log.d("KAKA::Burst", burst)

        val inputStream = if (participantId.startsWith("z", true)) resources.openRawResource(R.raw.survey_de) else resources.openRawResource(R.raw.survey)
//        val inputStream = if (participantId.startsWith("z", true)) resources.openRawResource(R.raw.tempsurvey_de) else resources.openRawResource(R.raw.tempsurvey)

        survey = GsonBuilder().create().fromJson<Questionnaire>(JsonReader(InputStreamReader(inputStream, "UTF-8")), Questionnaire::class.java)

        val textLabel = TextView(this)
        textLabel.id = R.id.text_label
        textLabel.layoutParams = params
        TextViewCompat.setTextAppearance(textLabel, android.R.style.TextAppearance_Large)
        questionnaire.addView(textLabel)

        val nextButton = Button(this)
        nextButton.id = R.id.next_button
        nextButton.tag = "next_button"
        nextButton.layoutParams = params
        questionnaire.addView(nextButton)

        startTime = Calendar.getInstance()
        startTime = Calendar.getInstance()
        newUserResponse["StartTime"] = startTime.time
        newUserResponse["Burst"] = burstID

        val lastDay = sharedPref!!.getLong("LastDay", startTime.timeInMillis)
        val lastDate = Calendar.getInstance()
        lastDate.timeInMillis = lastDay
        Log.d("LastDate", lastDate.toString())
        lastDayCheck = startTime.get(Calendar.YEAR) == lastDate.get(Calendar.YEAR) && startTime.get(Calendar.DAY_OF_YEAR) == lastDate.get(Calendar.DAY_OF_YEAR)

        if (sharedPref!!.getBoolean("HairSampling", false))
            sampling.add("HairSampling")
        if (sharedPref!!.getBoolean("SalivaSampling", false))
            sampling.add("SalivaSampling")
        loadQuestion(0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 555 && resultCode == Activity.RESULT_OK) {
            val bundle = data!!.extras
            userResponse[currentQuestion.tag + "Log"] = bundle.getString("log")
            userResponse[currentQuestion.tag + "TrialSizes"] = bundle.getString("trialSizes")
            userResponse[currentQuestion.tag + "TrialDotsCorrect"] = bundle.getString("trialDotsCorrect")
            userResponse[currentQuestion.tag + "TrialEuclideanScores"] = bundle.getString("trialEuclideanScores")
            userResponse[currentQuestion.tag + "TrialTimes"] = bundle.getString("trialTimes")
            userResponse[currentQuestion.tag + "DotsCorrect"] = bundle.getString("dotsCorrect")
            userResponse[currentQuestion.tag + "DotEuclideanScores"] = bundle.getString("dotEuclideanScores")
            userResponse[currentQuestion.tag + "DotsTimes"] = bundle.getString("dotTimes")
            userResponse[currentQuestion.tag + "MeanDots"] = bundle.getDouble("meanDots").toString()
            userResponse[currentQuestion.tag + "EuclideanScore"] = bundle.getDouble("euclideanScore").toString()
            userResponse[currentQuestion.tag + "Time"] = bundle.getLong("time").toString()
            evaluateResponse(bundle.getInt("questionId", survey.questions.size + 1))
            dotMemoryFlag = 1
        }
    }

    override fun onResume() {
        super.onResume()
        if (dotMemoryFlag == 1)
            dotMemoryFlag = 0
        else {
            val calendar = Calendar.getInstance()
            val currTime = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE)
            if (currTime > surveyTime + 20) {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setCancelable(false)
                alertDialog.setMessage(getString(R.string.sessionexpired))
                alertDialog.setPositiveButton(getString(R.string.ok)) { p0, p1 -> finish() }
                alertDialog.show()
            }
        }
    }

    private fun loadQuestion(questionId: Int) {
        val textLabel = this.findViewById<TextView>(R.id.text_label)
        val nextButton = findViewById<Button>(R.id.next_button)

        if (questionId >= survey.questions.size) {
            textLabel.text = getText(R.string.upld_msg)
            nextButton.text = getText(R.string.finish)
            nextButton.setOnClickListener { view ->
                kotlin.run {
                    newUserResponse["Responses"] = userResponse
                    endTime = Calendar.getInstance()
                    newUserResponse["EndTime"] = endTime.time
                    val time = TimeUnit.MILLISECONDS.toSeconds(endTime.timeInMillis - startTime.timeInMillis)
                    newUserResponse["Time"] = time

                    DBUtils.addSurvey(this, participantId, burst, surveyId, date, surveyTime, newUserResponse)
                    finish()
                }
            }
        } else {
            currentQuestion = survey.questions[questionId]
            Log.d("CurrentQuestion", currentQuestion.toString())
            val preConditions = currentQuestion.precondition
            Log.d("preConditions", (preConditions != null).toString())
            if (preConditions != null) {
                Log.d("preConditions", preConditions.toString())
                Log.d("dailyCount", (preConditions.containsKey("DailyCount") &&
                        preConditions["DailyCount"] as Double != dailyCount.toDouble()).toString() + "::" +
                        (preConditions["DailyCount"] as Double).toString() + "::" +
                        dailyCount.toDouble().toString())
                Log.d("lastDay", (preConditions.containsKey("LastDay") && !lastDayCheck)
                        .toString() + "::" + lastDayCheck.toString())
                Log.d("burst", (preConditions.containsKey("Burst") && !(preConditions["Burst"] as
                        ArrayList<String>).contains(burst)).toString())
                Log.d("sampling", (preConditions.containsKey("Sampling") && (preConditions["Sampling"] as
                        ArrayList<String>).intersect(sampling).isEmpty()).toString())
            }
            if (preConditions != null && (
                            (preConditions.containsKey("DailyCount") &&
                                    preConditions["DailyCount"] as Double != dailyCount.toDouble()) ||
                                    (preConditions.containsKey("LastDay") && !lastDayCheck) ||
                                    (preConditions.containsKey("Burst") &&
                                            !(preConditions["Burst"] as ArrayList<String>).contains(burst)) ||
                                    (preConditions.containsKey("Sampling") && (preConditions["Sampling"] as
                                            ArrayList<String>).intersect(sampling).isEmpty())
                            ))
                loadQuestion(questionId + 1)
            else {
                var question = currentQuestion.question
                question = question.replace("GOAL1", goal1)
                question = question.replace("GOAL2", goal2)
                question = question.replace("GOAL3", goal3)
                if (question.indexOf("<b>") == -1)
                    textLabel.text = question
                else
                    textLabel.text = formatQuestion(question)

                nextButton.text = getText(R.string.next)
                nextButton.isEnabled = false
                nextButton.setOnClickListener { view -> evaluateResponse(questionId) }

                responses = ArrayList()
                when (currentQuestion.type) {
                    "radio" -> addRadioButton()
                    "multiradio" -> addMultiRadioButton()
                    "checkbox" -> addCheckBox()
                    "multicheckbox" -> addMultiCheckBox()
                    "text" -> addEditText()
                    "game" -> {
                        nextButton.text = getString(R.string.start)
                        nextButton.isEnabled = true
                        nextButton.setOnClickListener { view ->
                            kotlin.run {
                                val intent = Intent(this, DotMemoryTaskESM::class.java)
                                intent.putExtra("questionId", questionId)
                                intent.putExtra("setsize", meanSetSize)
                                intent.putExtra("surveyTime", surveyTime)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivityForResult(intent, 555)
                            }
                        }
                    }
                    "instruction" -> nextButton.isEnabled = true
                }
            }
            questionnaireScrollView.post({ questionnaireScrollView.smoothScrollTo(0, 0) })
        }
    }

    private fun addRadioButton() {
        val radioGroup = RadioGroup(this)
        radioGroup.id = R.id.response
        radioGroup.layoutParams = params
        for (option in currentQuestion.options) {
            val radioButton = RadioButton(this)
            if (Build.VERSION.SDK_INT < 17)
                radioButton.id = generateViewId()
            if (!option.image.isNullOrEmpty()) {
                radioButton.setCompoundDrawablesWithIntrinsicBounds(
                        resources.getIdentifier(option.image, "drawable", this.packageName),
                        0, 0, 0)
                radioButton.compoundDrawablePadding = 16
                var left = 0
                if (Build.VERSION.SDK_INT < 17)
                    left = 32
                radioButton.setPadding(left, 8, 0, 8)
            }
            radioButton.text = option.option
            radioButton.tag = option.value
            TextViewCompat.setTextAppearance(radioButton, android.R.style.TextAppearance_Large)
            radioGroup.addView(radioButton)
        }
        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            findViewById<Button>(R.id.next_button).isEnabled = true
        }
        questionnaire.addView(radioGroup, 1)
    }

    private fun addMultiRadioButton() {
        val rowParams = TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        val cellParams = TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1F)

        val tableLayout = TableLayout(this)
        tableLayout.layoutParams = params

        val labelTableRow = TableRow(this)
        labelTableRow.layoutParams = rowParams
        for (optionfield in currentQuestion.optionfields) {
            val textLabel = TextView(this)
            textLabel.layoutParams = cellParams
            textLabel.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(textLabel, android.R.style.TextAppearance_Large)
            var label = optionfield.label
            label = label.replace("GOAL1", goal1)
            label = label.replace("GOAL2", goal2)
            label = label.replace("GOAL3", goal3)
            if (label.indexOf("<b>") == -1)
                textLabel.text = label
            else
                textLabel.text = formatQuestion(label)
            labelTableRow.addView(textLabel)
        }
        tableLayout.addView(labelTableRow)

        val startAnchorText = currentQuestion.anchortext["first"]
        if (!startAnchorText.isNullOrEmpty()) {
            val topAnchorTableRow = TableRow(this)
            topAnchorTableRow.layoutParams = rowParams
            val prependTextView = TextView(this)
            prependTextView.layoutParams = cellParams
            prependTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(prependTextView, android.R.style.TextAppearance_Large)
            prependTextView.text = getString(R.string.prepend_anchor)
            topAnchorTableRow.addView(prependTextView)
            val startTextView = TextView(this)
            startTextView.layoutParams = cellParams
            startTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(startTextView, android.R.style.TextAppearance_Large)
            startTextView.text = startAnchorText
            topAnchorTableRow.addView(startTextView)
            val appendTextView = TextView(this)
            appendTextView.layoutParams = cellParams
            appendTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(appendTextView, android.R.style.TextAppearance_Large)
            appendTextView.text = getString(R.string.append_anchor)
            topAnchorTableRow.addView(appendTextView)
            tableLayout.addView(topAnchorTableRow)
        }

        val radioTableRow = TableRow(this)
        radioTableRow.id = R.id.response
        radioTableRow.layoutParams = rowParams
        for (optionfield in currentQuestion.optionfields) {
            val radioGroup = RadioGroup(this)
            radioGroup.tag = optionfield.tag
            radioGroup.layoutParams = cellParams
            radioGroup.gravity = Gravity.CENTER
            for (option in currentQuestion.options) {
                val radioButton = RadioButton(this)
                if (Build.VERSION.SDK_INT < 17)
                    radioButton.id = generateViewId()
                if (!option.image.isNullOrEmpty()) {
                    radioButton.setCompoundDrawablesWithIntrinsicBounds(
                            resources.getIdentifier(option.image, "drawable", this.packageName),
                            0, 0, 0)
                    radioButton.compoundDrawablePadding = 16
                    var left = 0
                    if (Build.VERSION.SDK_INT < 17)
                        left = 32
                    radioButton.setPadding(left, 8, 0, 8)
                }
                radioButton.text = option.option
                radioButton.tag = option.value
                TextViewCompat.setTextAppearance(radioButton, android.R.style.TextAppearance_Large)
                radioGroup.addView(radioButton)
            }
            radioGroup.setOnCheckedChangeListener { radioGroup, i ->
                var bool = true
                val radioGroups = findViewById<LinearLayout>(R.id.response)
                (0 until radioGroups.childCount)
                        .asSequence()
                        .filter { radioGroups.getChildAt(it) is RadioGroup }
                        .map { radioGroups.getChildAt(it) as RadioGroup }
                        .forEach { bool = bool && (it.checkedRadioButtonId != -1) }
                findViewById<Button>(R.id.next_button).isEnabled = bool
            }
            radioTableRow.addView(radioGroup)
        }
        tableLayout.addView(radioTableRow)

        val endAnchorText = currentQuestion.anchortext["last"]
        if (!endAnchorText.isNullOrEmpty()) {
            val bottomAnchorTableRow = TableRow(this)
            bottomAnchorTableRow.layoutParams = rowParams
            val prependTextView = TextView(this)
            prependTextView.layoutParams = cellParams
            prependTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(prependTextView, android.R.style.TextAppearance_Large)
            prependTextView.text = getString(R.string.prepend_anchor)
            bottomAnchorTableRow.addView(prependTextView)
            val endTextView = TextView(this)
            endTextView.layoutParams = cellParams
            endTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(endTextView, android.R.style.TextAppearance_Large)
            endTextView.text = endAnchorText
            bottomAnchorTableRow.addView(endTextView)
            val appendTextView = TextView(this)
            appendTextView.layoutParams = cellParams
            appendTextView.gravity = Gravity.CENTER
            TextViewCompat.setTextAppearance(appendTextView, android.R.style.TextAppearance_Large)
            appendTextView.text = getString(R.string.append_anchor)
            bottomAnchorTableRow.addView(appendTextView)
            tableLayout.addView(bottomAnchorTableRow)
        }

        questionnaire.addView(tableLayout, 1)
    }

    private fun addCheckBox() {
        val linearLayout = LinearLayout(this)
        linearLayout.id = R.id.response
        linearLayout.layoutParams = params
        linearLayout.orientation = LinearLayout.VERTICAL
        for (option in currentQuestion.options) {
            val checkBox = CheckBox(this)
            checkBox.text = option.option
            checkBox.tag = option.value
            checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                kotlin.run {
                    val tag = buttonView.tag.toString()
                    if (isChecked)
                        responses.add(tag)
                    else
                        responses.remove(tag)
                    findViewById<Button>(R.id.next_button).isEnabled = responses.size > 0
                }
            }
            TextViewCompat.setTextAppearance(checkBox, android.R.style.TextAppearance_Large)
            linearLayout.addView(checkBox)
        }
        questionnaire.addView(linearLayout, 1)
    }

    private fun addMultiCheckBox() {
        multicheckResponses = HashMap()

        val rowParams = TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        val cellParams = TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1F)

        val tableLayout = TableLayout(this)
        tableLayout.layoutParams = params

        val labelTableRow = TableRow(this)
        labelTableRow.layoutParams = rowParams

        val emptyTextLabel = TextView(this)
        emptyTextLabel.layoutParams = cellParams
        TextViewCompat.setTextAppearance(emptyTextLabel, android.R.style.TextAppearance_Large)
        labelTableRow.addView(emptyTextLabel)
        for (optionfield in currentQuestion.optionfields) {
            val textLabel = TextView(this)
            textLabel.layoutParams = cellParams
            textLabel.gravity = Gravity.CENTER
            textLabel.setPadding(padding, padding, padding, padding)
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> textLabel.background = resources.getDrawable(R.drawable.multicheck_border, theme)
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> textLabel.background = resources.getDrawable(R.drawable.multicheck_border)
                else -> textLabel.setBackgroundDrawable(resources.getDrawable(R.drawable.multicheck_border))
            }
            TextViewCompat.setTextAppearance(textLabel, android.R.style.TextAppearance_Large)
            var label = optionfield.label
            label = label.replace("GOAL1", goal1)
            label = label.replace("GOAL2", goal2)
            label = label.replace("GOAL3", goal3)
            if (label.indexOf("<b>") == -1)
                textLabel.text = label
            else
                textLabel.text = formatQuestion(label)
            labelTableRow.addView(textLabel)
            multicheckResponses[optionfield.tag] = ArrayList()
        }
        tableLayout.addView(labelTableRow)

        for (option in currentQuestion.options) {
            val optionTableRow = TableRow(this)
            optionTableRow.layoutParams = rowParams

            val textLabel = TextView(this)
            textLabel.layoutParams = cellParams
            textLabel.gravity = Gravity.CENTER_VERTICAL
            textLabel.setPadding(padding, padding, padding, padding)
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> textLabel.background = resources.getDrawable(R.drawable.multicheck_border, theme)
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> textLabel.background = resources.getDrawable(R.drawable.multicheck_border)
                else -> textLabel.setBackgroundDrawable(resources.getDrawable(R.drawable.multicheck_border))
            }
            TextViewCompat.setTextAppearance(textLabel, android.R.style.TextAppearance_Large)
            textLabel.text = option.option
            optionTableRow.addView(textLabel)

            for (optionfield in currentQuestion.optionfields) {
                val checkBox = CheckBox(this)
                checkBox.layoutParams = cellParams
                checkBox.gravity = Gravity.CENTER
                checkBox.setPadding(padding, padding, padding, padding)
                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> checkBox.background = resources.getDrawable(R.drawable.multicheck_border, theme)
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> checkBox.background = resources.getDrawable(R.drawable.multicheck_border)
                    else -> checkBox.setBackgroundDrawable(resources.getDrawable(R.drawable.multicheck_border))
                }
                checkBox.tag = "${optionfield.tag}::${option.value}"
                checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                    kotlin.run {
                        val tag = checkBox.tag.toString()
                        val list = tag.split("::")
                        if (isChecked)
                            multicheckResponses[list[0]]?.add(list[1])
                        else
                            multicheckResponses[list[0]]?.remove(list[1])
                        var nextEnabled = true
                        for (key in multicheckResponses.keys) nextEnabled = nextEnabled and (multicheckResponses[key]?.size!! > 0)
                        findViewById<Button>(R.id.next_button).isEnabled = nextEnabled
                    }
                }
                TextViewCompat.setTextAppearance(checkBox, android.R.style.TextAppearance_Large)
                optionTableRow.addView(checkBox)
            }
            tableLayout.addView(optionTableRow)
        }

        questionnaire.addView(tableLayout, 1)
    }

    private fun addEditText() {
        val editText = EditText(this)
        editText.id = R.id.response
        editText.layoutParams = params
        editText.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val nextButton = findViewById<Button>(R.id.next_button)
                nextButton.isEnabled = p0.toString().trim().isNotEmpty()
            }
        })
        questionnaire.addView(editText, 1)
    }

    private fun evaluateResponse(questionId: Int) {
        var response = ""
        when (currentQuestion.type) {
            "radio" -> {
                val radioGroup = findViewById<RadioGroup>(R.id.response)
                val selectedId = radioGroup.checkedRadioButtonId
                response = findViewById<RadioButton>(selectedId).tag.toString()
                responses.add(response)
            }
            "multiradio" -> {
                val radioGroups = findViewById<LinearLayout>(R.id.response)
                for (i in 0 until radioGroups.childCount)
                    if (radioGroups.getChildAt(i) is RadioGroup) {
                        val radioGroup = radioGroups.getChildAt(i) as RadioGroup
                        val selectedId = radioGroup.checkedRadioButtonId
                        val radioVal = findViewById<RadioButton>(selectedId).tag.toString()
                        userResponse[radioGroup.tag.toString()] = radioVal
                    }
            }
            "checkbox" -> response = responses.joinToString(", ")
            "multicheckbox" -> {
                for (optionField in currentQuestion.optionfields)
                    userResponse[optionField.tag] = multicheckResponses[optionField.tag]?.joinToString(", ")!!
            }
            "text" -> {
                hideSoftInput()
                val editText = findViewById<EditText>(R.id.response)
                response = editText.text.toString()
            }
        }

        if (!listOf("instruction", "game").contains(currentQuestion.type))
            questionnaire.removeViewAt(1)
        if (currentQuestion.type in listOf("radio", "checkbox", "text"))
            userResponse[currentQuestion.tag] = response

        var nextQuestion = questionId + 1
        if (currentQuestion.nextquestion == 0
                && currentQuestion.type in listOf("radio", "checkbox")
                && currentQuestion.conditions != null) {
            for ((value, nextquestion) in currentQuestion.conditions) {
                if (value in responses) {
                    nextQuestion = nextquestion
                    break
                }
            }
        } else if (currentQuestion.nextquestion != 0) {
            nextQuestion = currentQuestion.nextquestion
        }
        loadQuestion(nextQuestion)
    }

    private fun hideSoftInput() {
        val view = this.currentFocus
        if (view != null) {
            val iMM = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            iMM.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onBackPressed() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setCancelable(false)
        alertDialog.setMessage(getString(R.string.back_pressed))
        alertDialog.setPositiveButton(getString(R.string.ok)) { p0, p1 -> finish() }
        alertDialog.setNegativeButton(getString(R.string.cancel)) { p0, p1 -> }
        alertDialog.show()
    }

    private val sNextGeneratedId = AtomicInteger(1)
    private fun generateViewId(): Int {
        while (true) {
            val result = sNextGeneratedId.get()
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            var newValue = result + 1
            if (newValue > 0x00FFFFFF)
                newValue = 1 // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue))
                return result
        }
    }

    private fun formatQuestion(question: String): SpannableStringBuilder {
        var boldFlag = true
        val spannableStringBuilder = SpannableStringBuilder(question)
        while (boldFlag) {
            var startIndex = spannableStringBuilder.indexOf("<b>")
            if (startIndex == -1)
                boldFlag = false
            else {
                startIndex += 3
                var endIndex = spannableStringBuilder.indexOf("</b>")
                if (endIndex == -1)
                    endIndex = spannableStringBuilder.length
                spannableStringBuilder.setSpan(StyleSpan(Typeface.BOLD), startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                spannableStringBuilder.replace(startIndex - 3, startIndex, "")
                endIndex = spannableStringBuilder.indexOf("</b>")
                if (endIndex != -1)
                    spannableStringBuilder.replace(endIndex, endIndex + 4, "")
            }
        }

        return spannableStringBuilder
    }
}