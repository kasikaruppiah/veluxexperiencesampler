package edu.northeastern.ledlab.veluxexperiencesampler

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.firebase.jobdispatcher.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.max

class NotifyJob : JobService() {
    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "NotifyJob::JobService::onStartJob")

        val extras = job!!.extras
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        var mNotificationId = extras!!.getInt("mNotificationId", Integer.MIN_VALUE)
        if (mNotificationId == Integer.MIN_VALUE) {
            mNotificationId = System.currentTimeMillis().toInt()
            extras.putInt("mNotificationId", mNotificationId)

            val prefEditor = sharedPref.edit()
            prefEditor.putBoolean("APPOPENEDAFTERNOTIFICATION", false)
            prefEditor.apply()
        }

        val mNotifyMgr = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val timeInMillis = System.currentTimeMillis()
        val maxTimeInMillis = extras.getLong("timeInMillis", System.currentTimeMillis()) + TimeUnit.MINUTES.toMillis(20)

        if (timeInMillis < maxTimeInMillis) {
            if (!sharedPref.getBoolean("APPOPENEDAFTERNOTIFICATION", false)) {
                val text = extras.getString("text", this.getString(R.string.default_notification))
                val soundURI = Uri.parse("android.resource://edu.northeastern.ledlab.veluxexperiencesampler/" + R.raw.notification)
                val resumeIntent = Intent(this, MainActivity::class.java)
                val stackBuilder = android.support.v4.app.TaskStackBuilder.create(this)
                stackBuilder.addParentStack(MainActivity::class.java)
                stackBuilder.addNextIntent(resumeIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

                val mBuilder = NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(this.getString(R.string.app_name))
                        .setContentText(text)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setAutoCancel(true)
                        .setSound(soundURI)
                        .setVibrate(longArrayOf(1000, 1000))
                        .setContentIntent(resultPendingIntent)
                        .build()

                mNotifyMgr.notify(mNotificationId, mBuilder)

                val timeInSeconds = TimeUnit.MINUTES.toSeconds(4L).toInt()
                val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))

                val myJob = dispatcher.newJobBuilder()
                        .setService(NotifyJob::class.java)
                        .setTag("${NotifyJob.TAG}_multiple_$timeInMillis")
                        .setRecurring(false)
                        .setLifetime(Lifetime.FOREVER)
                        .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                        .setReplaceCurrent(true)
                        .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                        .setExtras(extras)
                        .build()

                dispatcher.mustSchedule(myJob)
            } else
                mNotifyMgr.cancel(mNotificationId)
        } else
            mNotifyMgr.cancel(mNotificationId)

        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "NotifyJob::JobService::onStopJob")

        return true
    }

    companion object {
        const val TAG = "notify_job"
        private val random = Random()

        private fun scheduleJob(dispatcher: FirebaseJobDispatcher, timeInMillis: Long, text: String) {
            Log.d("KAKA", "NotifyJob::CompanionObject::scheduleJob")

            val extras = Bundle()
            extras.putString("text", text)
            extras.putLong("timeInMillis", timeInMillis)

            val timeInSeconds = TimeUnit.MILLISECONDS.toSeconds(max(60000L, timeInMillis - System.currentTimeMillis())).toInt()

            val myJob = dispatcher.newJobBuilder()
                    .setService(NotifyJob::class.java)
                    .setTag("${NotifyJob.TAG}$timeInMillis")
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .setExtras(extras)
                    .build()

            dispatcher.mustSchedule(myJob)
        }

        fun scheduleJobs(context: Context) {
            Log.d("KAKA", "NotifyJob::CompanionObject::scheduleJobs")

            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))

            val today = Calendar.getInstance().time

            val newCal = Calendar.getInstance()
            newCal.time = today
            newCal.set(Calendar.HOUR_OF_DAY, 0)
            newCal.set(Calendar.MINUTE, 0)
            newCal.set(Calendar.SECOND, 0)
            newCal.set(Calendar.MILLISECOND, 0)

            var startDate = today
            var endDate = today
            var samplingStartDate = today
            var samplingEndDate = today

            val timeMap = HashMap<String, DayMap>()
            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy")

            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val weekdayWake = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
            val weekdaySleep = weekdayWake + 720
            val weekendWake = sharedPref.getInt(context.getString(R.string.weekend_wake_key), 480)
            val weekendSleep = weekendWake + 720
            val sampling = sharedPref.getBoolean("SalivaSampling", false)

            for (i in 1..5) {
                newCal.add(Calendar.DATE, 1)
                val date = newCal.time
                if (i == 1)
                    startDate = date
                timeMap[simpleDateFormat.format(date)] = scheduleNotificationForDay(context, dispatcher, newCal, weekdayWake, weekdaySleep, weekendWake, weekendSleep)
                if (sampling)
                    when (i) {
                        1 -> {
                            val samplingTime = newCal.timeInMillis + TimeUnit.HOURS.toMillis(18)
                            SamplingNotifyJob.scheduleJob(dispatcher, samplingTime, context.getString(R.string.remainder_a))
                        }
                        2, 3 -> {
                            val penalityJobTime = newCal.clone() as Calendar
                            penalityJobTime.set(Calendar.HOUR_OF_DAY, 23)
                            penalityJobTime.set(Calendar.MINUTE, 0)

                            SamplingPenaltyJob.scheduleJob(dispatcher, penalityJobTime.timeInMillis)
                            if (i == 2)
                                samplingStartDate = date
                        }
                        4 -> samplingEndDate = date
                    }
                if (i == 5) {
                    newCal.add(Calendar.DATE, 1)
                    endDate = newCal.time
                    LastDayJob.scheduleJob(dispatcher, newCal.timeInMillis)
                }
            }

            val burstTimeMap = TimeMap(timeMap, startDate, endDate, samplingStartDate, samplingEndDate)

            DBUtils.addBurst(context, burstTimeMap)
        }

        private fun rand(from: Int, to: Int): Int {
            return random.nextInt(to - from) + from
        }

        private fun getSurveyTime(startTime: Int, endTime: Int): ArrayList<Int> {
            val surveyTimes = ArrayList<Int>()
            val interval = (endTime - startTime) / 6
            for (i in 1..6) {
                val surveyStartTime = startTime + interval * (i - 1)
                val surveyEndTime = startTime + interval * i - 40
                val surveyTime = rand(surveyStartTime, surveyEndTime)
                surveyTimes.add(surveyTime)
            }
            return surveyTimes
        }

        fun scheduleNotificationForDay(context: Context, dispatcher: FirebaseJobDispatcher, calendar: Calendar, weekdayWake: Int, weekdaySleep: Int, weekendWake: Int, weekendSleep: Int): DayMap {
            val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
            val surveyTimes = when (dayOfWeek) {
                in arrayOf(Calendar.SATURDAY, Calendar.SUNDAY) -> getSurveyTime(weekendWake, weekendSleep)
                else -> getSurveyTime(weekdayWake, weekdaySleep)
            }

            var i = 0
            for (surveyTime in surveyTimes) {
                val newCalendar = calendar.clone() as Calendar
                newCalendar.set(Calendar.HOUR_OF_DAY, surveyTime / 60)
                newCalendar.set(Calendar.MINUTE, surveyTime % 60)
                i++
                scheduleJob(dispatcher, newCalendar.timeInMillis, String.format(context.getString(R.string.notification), i, 6))
            }

            PenalityJob.scheduleJob(dispatcher, calendar.timeInMillis, max(weekdaySleep, weekendSleep))

            return DayMap(surveyTimes)
        }
    }
}