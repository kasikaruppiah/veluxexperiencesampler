package edu.northeastern.ledlab.veluxexperiencesampler

import android.os.Bundle
import android.util.Log
import com.firebase.jobdispatcher.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.max

class SamplingPenaltyJob : JobService() {
    override fun onStartJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SamplingPenaltyJob::JobService::onStartJob")

        val extras = job!!.extras
        val timeInMillis = extras!!.getLong("timeInMillis", Calendar.getInstance().timeInMillis)

        DBUtils.scheduleSamplingPenaltyJob(this, timeInMillis)

        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        Log.d("KAKA", "SamplingPenaltyJob::JobService::onStopJob")

        return true
    }

    companion object {
        private const val TAG = "sampling_penality_job"

        fun scheduleJob(dispatcher: FirebaseJobDispatcher, timeInMillis: Long) {
            val timeInSeconds = TimeUnit.MILLISECONDS.toSeconds(max(60000L, timeInMillis - System.currentTimeMillis())).toInt()

            val extras = Bundle()
            extras.putLong("timeInMillis", timeInMillis)

            val myJob = dispatcher.newJobBuilder()
                    .setService(SamplingPenaltyJob::class.java)
                    .setTag("${SamplingPenaltyJob.TAG}$timeInMillis")
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setTrigger(Trigger.executionWindow(timeInSeconds - 30, timeInSeconds))
                    .setReplaceCurrent(true)
                    .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                    .setExtras(extras)
                    .build()

            dispatcher.mustSchedule(myJob)
        }
    }
}