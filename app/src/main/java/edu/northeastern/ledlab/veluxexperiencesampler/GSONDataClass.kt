package edu.northeastern.ledlab.veluxexperiencesampler

import java.util.*

data class TimeMap(val timeMap: HashMap<String, DayMap>, val startDate: Date, val endDate: Date, val samplingStartDate: Date, val samplingEndDate: Date)

data class DayMap(val surveyTimes: ArrayList<Int>,
                  val completedSurveys: HashSet<Int> = HashSet(),
                  val completedProofs: HashSet<Int> = HashSet(),
                  val proofTimes: ArrayList<Int>? = null,
                  val wakeUpTime: Date? = null,
                  val sleepHours: Int? = null)

data class SurveyCheck(val check: Boolean, val date: String?, val time: Int?, val dailyCount: Int = 0)

