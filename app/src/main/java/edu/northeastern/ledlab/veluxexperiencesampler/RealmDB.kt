package edu.northeastern.ledlab.veluxexperiencesampler

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class SamplingProof(
        @PrimaryKey var proofID: Int = 0,
        var imageFileLocation: String = "",
        var uploadDate: Date? = null
) : RealmObject()